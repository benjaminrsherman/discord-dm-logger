use failure::Error;
use rusqlite::{Connection, OptionalExtension};
use serenity::{
    builder::CreateEmbed,
    http::raw::Http,
    model::{
        channel::{Channel, ChannelType, Group, GuildChannel, Message, PrivateChannel},
        event::MessageUpdateEvent,
        gateway::Ready,
        id::{ChannelId, MessageId},
    },
    prelude::*,
};
use std::convert::TryFrom;
use std::sync::{Arc, Mutex};

mod config;
mod utils;

fn get_guild_log_channel(
    conn: &mut Connection,
    user_http: &Http,
    bot_http: &Http,
    source_chann: &GuildChannel,
) -> Result<Option<ChannelId>, Error> {
    let conn_res: Option<isize> = conn
        .query_row(
            "SELECT dest_category_id FROM guild_associations WHERE source_guild_id = (?1)",
            &[source_chann.guild_id.to_string()],
            |row| row.get(0),
        )
        .optional()?;

    let dest_category_id = match conn_res {
        None => return Ok(None),
        Some(dest_channel_id) => u64::try_from(dest_channel_id)?,
    };

    let dest_category = if dest_category_id == 0 {
        let created_category = config::guild_server_id().create_channel(&bot_http, |c| {
            let name = match source_chann.guild_id.to_partial_guild(&user_http) {
                Ok(guild) => guild.name,
                _ => String::from("UNNAMED_GUILD"),
            };

            c.name(name).kind(ChannelType::Category)
        })?;

        conn.execute(
            "UPDATE guild_associations SET dest_category_id = ?1 WHERE source_guild_id = ?2",
            &[
                created_category.id.to_string(),
                source_chann.guild_id.to_string(),
            ],
        )?;

        created_category
    } else {
        config::guild_server_id()
            .channels(&bot_http)?
            .get(&ChannelId::from(dest_category_id))
            .expect("Error finding guild category") // TODO: implement error handling
            .clone()
    };

    let dest_channel = config::guild_server_id().create_channel(&bot_http, |c| {
        c.name(source_chann.name.clone())
            .kind(ChannelType::Text)
            .category(dest_category)
    })?;

    conn.execute(
        "INSERT INTO channel_associations (source_channel_id, dest_channel_id) VALUES (?1, ?2)",
        &[source_chann.id.to_string(), dest_channel.id.to_string()],
    )?;

    Ok(Some(dest_channel.id))
}

fn get_group_log_channel(
    conn: &mut Connection,
    bot_http: &Http,
    source_chann: &Group,
) -> Result<Option<ChannelId>, Error> {
    let conn_res: Option<isize> = conn
        .query_row(
            "SELECT dest_channel_id FROM channel_associations WHERE source_channel_id = (?1)",
            &[source_chann.channel_id.to_string()],
            |row| row.get(0),
        )
        .optional()?;

    let dest_channel_id = match conn_res {
        Some(val) => ChannelId::from(u64::try_from(val)?),
        None => {
            let name = source_chann
                .recipients
                .iter()
                .map(|(_, recipient)| recipient.read().name.clone())
                .collect::<Vec<String>>()
                .join(", ");

            let topic = source_chann
                .recipients
                .iter()
                .map(|(_, recipient)| recipient.read().id.to_string())
                .collect::<Vec<String>>()
                .join(", ");

            let id = config::dm_server_id()
                .create_channel(&bot_http, |c| c.name(name).topic(topic))?
                .id;

            conn.execute("INSERT INTO channel_associations (source_channel_id, dest_channel_id) VALUES (?1, ?2)",
                &[source_chann.channel_id.to_string(), id.to_string()])?;

            id
        }
    };

    Ok(Some(dest_channel_id))
}

fn get_dm_log_channel(
    conn: &mut Connection,
    bot_http: &Http,
    source_chann: &PrivateChannel,
) -> Result<Option<ChannelId>, Error> {
    let conn_res: Option<isize> = conn
        .query_row(
            "SELECT dest_channel_id FROM channel_associations WHERE source_channel_id = ?1",
            &[source_chann.id.to_string()],
            |row| row.get(0),
        )
        .optional()?;

    let dest_channel_id = match conn_res {
        Some(val) => ChannelId::from(u64::try_from(val)?),
        None => {
            let id = config::dm_server_id()
                .create_channel(&bot_http, |c| {
                    let name = source_chann.recipient.read().name.clone();
                    let topic = source_chann.recipient.read().id;
                    c.name(name).topic(topic)
                })?
                .id;

            conn.execute("INSERT INTO channel_associations (source_channel_id, dest_channel_id) VALUES (?1, ?2)",
                &[source_chann.id.to_string(), id.to_string()]).unwrap();

            id
        }
    };

    Ok(Some(dest_channel_id))
}

fn get_log_channel(
    conn: &mut Connection,
    user_http: &Http,
    bot_http: &Http,
    chann: &Channel,
) -> Result<Option<ChannelId>, Error> {
    match chann {
        Channel::Guild(channel) => {
            get_guild_log_channel(conn, user_http, bot_http, &channel.read())
        }
        Channel::Group(group) => get_group_log_channel(conn, bot_http, &group.read()),
        Channel::Private(dm) => get_dm_log_channel(conn, bot_http, &dm.read()),
        _ => {
            panic!("Received message from unknown channel type");
        }
    }
}

fn log_message_with_channel_id(
    conn: &mut Connection,
    user_http: &Http,
    bot_http: &Http,
    dest_channel_id: &ChannelId,
    msg: Option<&Message>,
    content: &str,
) -> Result<(), Error> {
    let conn_res: Option<isize> = conn
        .query_row(
            "SELECT dest_channel_id FROM channel_associations WHERE source_channel_id = ?1",
            &[dest_channel_id.to_string()],
            |row| row.get(0),
        )
        .optional()?;

    let dest_channel_id = match conn_res {
        Some(channel_id) => ChannelId::from(u64::try_from(channel_id)?),
        None => match get_log_channel(
            conn,
            user_http,
            bot_http,
            &dest_channel_id.to_channel(user_http)?,
        )? {
            Some(chann_id) => chann_id,
            None => return Ok(()),
        },
    };

    match msg {
        None => {
            dest_channel_id.say(bot_http, content)?;
        }
        Some(msg) => {
            let attachments_intermediate: Vec<(Vec<u8>, String)> = msg
                .attachments
                .iter()
                .map(|a| (a.download(), a.filename.clone()))
                .filter(|(a, _)| a.is_ok())
                .map(|(a, fname)| (a.unwrap(), fname))
                .collect();

            dest_channel_id.send_message(bot_http, |m| {
                let attachments: Vec<(&[u8], &str)> = attachments_intermediate
                    .iter()
                    .map(|(a, fname)| (a.as_slice(), fname.as_str()))
                    .collect();

                m.files(attachments);

                if !msg.embeds.is_empty() {
                    let embed = msg.embeds[0].clone();
                    m.embed(|e| {
                        *e = CreateEmbed::from(embed);
                        e
                    });
                };

                m.content(content)
            })?;
        }
    };

    Ok(())
}

fn log_message(
    conn: &mut Connection,
    user_http: &Http,
    bot_http: &Http,
    msg: &Message,
    content: &str,
) -> Result<(), Error> {
    let dest_channel_id = msg.channel_id;

    log_message_with_channel_id(
        conn,
        user_http,
        bot_http,
        &dest_channel_id,
        Some(msg),
        content,
    )
}

struct ConnectionKey;
impl TypeMapKey for ConnectionKey {
    type Value = Arc<Mutex<Connection>>;
}
struct HttpKey;
impl TypeMapKey for HttpKey {
    type Value = Arc<Http>;
}

struct BotHandler;
impl EventHandler for BotHandler {}
struct UserHandler;
impl EventHandler for UserHandler {
    fn ready(&self, _: Context, ready: Ready) {
        println!("{} is connected!", ready.user.name);
        utils::send_pushover_msg("Discord CYA", "Logger online!", false);
    }

    fn message(&self, ctx: Context, msg: Message) {
        let data = ctx.data.read();

        let conn = data
            .get::<ConnectionKey>()
            .expect("Expected Connection in ShareMap");

        let bot_http = data.get::<HttpKey>().expect("Expected Http in ShareMap");

        let msg_res = log_message(
            &mut conn.lock().unwrap(),
            &ctx.http,
            &bot_http,
            &msg,
            &format!("{}: {}", msg.author.name, msg.content_safe(&ctx)),
        );

        if let Err(e) = msg_res {
            utils::send_pushover_msg("Discord CYA error", &format!("{:?}", e), true);
            eprintln!("{:?}", e);
        }

        if let Some(_) = msg.channel_id.to_channel(&ctx).unwrap().private() {
            if !msg.is_own(&ctx) {
                utils::send_pushover_msg(
                    &format!("{}", msg.author.name),
                    &format!("{}", msg.content_safe(&ctx)),
                    false,
                );
            }
        }
    }

    fn message_delete(&self, ctx: Context, channel_id: ChannelId, msg_id: MessageId) {
        let data = ctx.data.read();

        let conn = data
            .get::<ConnectionKey>()
            .expect("Expected Connection in ShareMap");

        let bot_http = data.get::<HttpKey>().expect("Expected Http in ShareMap");

        //let deleted_message = ctx.cache.read().message(channel_id, msg_id);

        let msg_to_send = match ctx.cache.read().message(channel_id, msg_id) {
            Some(msg) => {
                if msg.is_own(&ctx) {
                    return;
                }
                format!("DELETED: {}: {}", msg.author.name, msg.content_safe(&ctx))
            }
            None => String::from("DELETED: Unknown"),
        };

        let msg_res = log_message_with_channel_id(
            &mut conn.lock().unwrap(),
            &ctx.http,
            &bot_http,
            &channel_id,
            None,
            &msg_to_send,
        );

        if let Err(e) = msg_res {
            utils::send_pushover_msg("Discord CYA error", &format!("{:?}", e), true);
            eprintln!("{:?}", e);
        }

        if let Some(chann) = channel_id.to_channel(&ctx).unwrap().private() {
            utils::send_pushover_msg(
                &format!("DELETED: {}", &chann.read().name()),
                &msg_to_send[9..],
                false,
            );
        } else if let Some(chann) = channel_id.to_channel(&ctx).unwrap().group() {
            utils::send_pushover_msg(
                &format!("DELETED: {}", &chann.read().name()),
                &msg_to_send[9..],
                false,
            );
        }
    }

    fn message_update(
        &self,
        ctx: Context,
        old_msg: Option<Message>,
        new_msg: Option<Message>,
        event: MessageUpdateEvent,
    ) {
        let data = ctx.data.read();

        let conn = data
            .get::<ConnectionKey>()
            .expect("Expected Connection in ShareMap");

        let bot_http = data.get::<HttpKey>().expect("Expected Http in ShareMap");

        let author_name = match &old_msg {
            Some(msg) => &msg.author.name,
            None => match &new_msg {
                Some(msg) => &msg.author.name,
                None => "UNKNOWN",
            },
        };

        let msg = format!(
            "(EDITED) {}:\n{}\n\n{}",
            author_name,
            match &old_msg {
                Some(msg) => msg.content_safe(&ctx),
                None => String::from("UNKNOWN"),
            },
            match &new_msg {
                Some(msg) => msg.content_safe(&ctx),
                None => String::from("UNKNOWN"),
            }
        );

        let msg_res = log_message_with_channel_id(
            &mut conn.lock().unwrap(),
            &ctx.http,
            &bot_http,
            &event.channel_id,
            new_msg.as_ref(),
            &msg,
        );

        if let Err(e) = msg_res {
            utils::send_pushover_msg("Discord CYA error", &format!("{:?}", e), true);
            eprintln!("{:?}", e);
        }
    }
}

fn main() {
    let bot_client_http = Client::new(&config::BOT_TOKEN, BotHandler)
        .expect("Err creating bot client")
        .cache_and_http
        .http
        .clone();

    let mut user_client =
        Client::new(&config::USER_TOKEN, UserHandler).expect("Err creating bot client");

    let conn = Connection::open("database.db").unwrap();

    {
        let mut data = user_client.data.write();

        data.insert::<ConnectionKey>(Arc::new(Mutex::new(conn)));
        data.insert::<HttpKey>(bot_client_http);
    }

    if let Err(why) = user_client.start() {
        println!("Client error: {:?}", why);
    }
}
