use crate::config;
use backtrace;
use reqwest;

pub fn send_pushover_msg(title: &str, msg: &str, err: bool) {
    let params = [
        ("token", config::PUSHOVER_APP_TOKEN),
        ("user", config::PUSHOVER_USER_TOKEN),
        ("title", title),
        ("message", msg),
    ];
    let client = reqwest::Client::new();
    match client
        .post("https://api.pushover.net/1/messages.json")
        .form(&params)
        .send()
    {
        Err(e) => eprintln!("{}", e),
        _ => {}
    };

    if err {
        println!("{:?}", backtrace::Backtrace::new());
    }
}
