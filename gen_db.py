import sys
import os
import sqlite3
import json

connection = sqlite3.connect("database.db")
c = connection.cursor()

c.execute(
    """
    CREATE TABLE IF NOT EXISTS channel_associations (
    source_channel_id INTEGER DEFAULT 0,
    dest_channel_id INTEGER DEFAULT 0
    );
    """
)
connection.commit()

c.execute(
    """
    CREATE TABLE IF NOT EXISTS guild_associations (
    source_guild_id INTEGER DEFAULT 0,
    dest_category_id INTEGER DEFAULT 0
    );
    """
)
connection.commit()
