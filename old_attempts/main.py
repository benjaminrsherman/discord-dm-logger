import discord
from dotenv import load_dotenv
import os
import http.client, urllib
import threading
import sqlite3
import traceback

load_dotenv()
SELF_ID = int(os.getenv("SELF_ID"))

client = discord.Client()
bot = discord.Client()

db_lock = threading.Lock()
db_connection = sqlite3.connect("database.db")
db_c = db_connection.cursor()

guild_log_server = None
dm_log_server = None


@bot.event
async def on_ready():
    global guild_log_server
    global dm_log_server
    print("Bot client online")
    guild_log_server = bot.get_guild(int(os.getenv("GUILD_LOG_SERVER_ID")))
    dm_log_server = bot.get_guild(int(os.getenv("DM_LOG_SERVER_ID")))


@client.event
async def on_ready():
    print("Main client online")
    pushover = http.client.HTTPSConnection("api.pushover.net:443")
    pushover.request(
        "POST",
        "/1/messages.json",
        urllib.parse.urlencode(
            {
                "token": os.getenv("PUSHOVER_APP_TOKEN"),
                "user": os.getenv("PUSHOVER_USER_TOKEN"),
                "message": "Discord CYA is online",
            }
        ),
        {"Content-type": "application/x-www-form-urlencoded"},
    )
    pushover.getresponse()


async def log_message(msg, content):
    global db_lock
    global db_c
    global db_connection
    global guild_log_server
    global dm_log_server
    global bot

    try:
        with db_lock:
            db_c.execute(
                f"SELECT dest_channel_id FROM channel_associations WHERE source_channel_id = {msg.channel.id}"
            )
            dest_channel_id = db_c.fetchone()

            if (
                dest_channel_id is None
            ):  # We need to create a new channel to log this with
                if (
                    msg.channel.type is not discord.ChannelType.private
                    and msg.channel.type is not discord.ChannelType.group
                ):  # This is in a guild, so we should log it in the guild server
                    guild_id = msg.channel.guild.id
                    db_c.execute(
                        f"SELECT dest_category_id FROM guild_associations WHERE source_guild_id = {guild_id}"
                    )
                    dest_category_id = db_c.fetchone()
                    if dest_category_id is None:
                        return
                    if dest_category_id == 0:
                        dest_category = guild_log_server.create_category(msg.guild.name)
                        db_c.execute(
                            f"UPDATE guild_associations SET dest_category_id = {dest_category.id} WHERE source_guild_id = {guild_id}"
                        )
                        db_connection.commit()
                    else:
                        dest_category = guild_log_server.get_channel(dest_category_id)
                        destination_channel = guild_log_server.create_text_channel(
                            msg.channel.name, category=dest_category
                        )
                        db_c.execute(
                            f"INSERT INTO channel_associations (source_channel_id, dest_channel_id) VALUES ({msg.channel.id}, {destination_channel.id})"
                        )
                        db_connection.commit()
                else:  # This is a private message, so we should log it in the private server
                    try:
                        name = msg.channel.name
                    except:
                        try:
                            name = msg.channel.recipient
                        except:
                            name = ", ".join(
                                [recipient.name for recipient in msg.channel.recipients]
                            )
                    try:
                        topic = str(msg.channel.recipient.id)
                    except:
                        topic = ", ".join(
                            [recipient.id for recipient in msg.channel.recipients]
                        )
                    destination_channel = dm_log_server.create_text_channel(
                        name, topic=topic
                    )
                    db_c.execute(
                        f"INSERT INTO channel_associations (source_channel_id, dest_channel_id) VALUES ({msg.channel.id}, {destination_channel.id})"
                    )
                    db_c.commit()
            else:
                destination_channel = bot.get_channel(dest_channel_id[0])

        destination_channel.send(content)
    except:
        pushover = http.client.HTTPSConnection("api.pushover.net:443")
        pushover.request(
            "POST",
            "/1/messages.json",
            urllib.parse.urlencode(
                {
                    "token": os.getenv("PUSHOVER_APP_TOKEN"),
                    "user": os.getenv("PUSHOVER_USER_TOKEN"),
                    "title": "Discord CYA Error",
                    "message": traceback.format_exc(),
                }
            ),
            {"Content-type": "application/x-www-form-urlencoded"},
        )
        pushover.getresponse()


@client.event
async def on_message(msg):
    if msg.author.id != SELF_ID:
        await log_message(msg, f"{msg.author.name}: {msg.clean_content}")
        if msg.channel.type is discord.ChannelType.private:
            print(f"{msg.clean_content}")
            pushover = http.client.HTTPSConnection("api.pushover.net:443")
            pushover.request(
                "POST",
                "/1/messages.json",
                urllib.parse.urlencode(
                    {
                        "token": os.getenv("PUSHOVER_APP_TOKEN"),
                        "user": os.getenv("PUSHOVER_USER_TOKEN"),
                        "title": msg.author.name,
                        "message": msg.clean_content,
                    }
                ),
                {"Content-type": "application/x-www-form-urlencoded"},
            )
            pushover.getresponse()


@client.event
async def on_message_delete(msg):
    if msg.author.id != SELF_ID:
        await log_message(msg, f"(DELETED) {msg.author.name}: {msg.clean_content}")
        if msg.channel.type is discord.ChannelType.private:
            pushover = http.client.HTTPSConnection("api.pushover.net:443")
            pushover.request(
                "POST",
                "/1/messages.json",
                urllib.parse.urlencode(
                    {
                        "token": os.getenv("PUSHOVER_APP_TOKEN"),
                        "user": os.getenv("PUSHOVER_USER_TOKEN"),
                        "title": f"{msg.author.name} deleted a message",
                        "message": msg.clean_content,
                    }
                ),
                {"Content-type": "application/x-www-form-urlencoded"},
            )
            pushover.getresponse()


@client.event
async def on_message_edit(before, after):
    if before.author.id != SELF_ID:
        await log_message(
            msg,
            f"(EDITED) {before.author.name}:\n{before.clean_content}\n\n{after.clean_content}",
        )
        if before.channel.type is discord.ChannelType.private:
            pushover = http.client.HTTPSConnection("api.pushover.net:443")
            pushover.request(
                "POST",
                "/1/messages.json",
                urllib.parse.urlencode(
                    {
                        "token": os.getenv("PUSHOVER_APP_TOKEN"),
                        "user": os.getenv("PUSHOVER_USER_TOKEN"),
                        "title": f"{before.author.name} edited a message",
                        "message": f"{before.clean_content} -> {after.clean_content}",
                    }
                ),
                {"Content-type": "application/x-www-form-urlencoded"},
            )
            pushover.getresponse()


def send_traceback():
    pushover = http.client.HTTPSConnection("api.pushover.net:443")
    pushover.request(
        "POST",
        "/1/messages.json",
        urllib.parse.urlencode(
            {
                "token": os.getenv("PUSHOVER_APP_TOKEN"),
                "user": os.getenv("PUSHOVER_USER_TOKEN"),
                "title": "Discord CYA Error",
                "message": traceback.format_exc(),
            }
        ),
        {"Content-type": "application/x-www-form-urlencoded"},
    )
    pushover.getresponse()


@client.event
async def on_error(event, *args, **kwargs):
    send_traceback()


@bot.event
async def on_error(event, *args, **kwargs):
    send_traceback()


async def init_bot():
    await bot.start(os.getenv("BOT_TOKEN"))


import asyncio
from collections import namedtuple

# First, we must attach an event signalling when the bot has been
# closed to the client itself so we know when to fully close the event loop.

Entry = namedtuple("Entry", "client event")
entries = [
    (Entry(client=bot, event=asyncio.Event()), os.getenv("BOT_TOKEN"), True),
    (Entry(client=client, event=asyncio.Event()), os.getenv("CLIENT_TOKEN"), False),
]

# Then, we should login to all our clients and wrap the connect call
# so it knows when to do the actual full closure

loop = asyncio.get_event_loop()


async def login():
    for (e, token, bot) in entries:
        await e.client.login(token, bot=bot)


async def wrapped_connect(entry_tuple):
    (entry, _, _) = entry_tuple
    try:
        await entry.client.connect()
    except Exception as e:
        await entry.client.close()
        print("We got an exception: ", e.__class__.__name__, e)
        entry.event.set()


# actually check if we should close the event loop:
async def check_close():
    futures = [e.event.wait() for (e, _, _) in entries]
    await asyncio.wait(futures)


# here is when we actually login
loop.run_until_complete(login())

# now we connect to every client
for entry in entries:
    loop.create_task(wrapped_connect(entry))

# now we're waiting for all the clients to close
loop.run_until_complete(check_close())

# finally, we close the event loop
loop.close()
