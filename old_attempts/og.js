/* ORIGINAL LOGGER, WRITTEN IN JAVASCRIPT */
/* KEPT FOR ARCHIVAL REASONS ONLY */
require('dotenv').config();

const http = require('http');
const express = require('express');
var fs = require('fs');
var request = require('request');
var schedule = require('node-schedule');
const app = express();
app.get("/", (request, response) => {
	response.sendStatus(200);
});
app.listen("3000");
setInterval(() => {
	http.get('http://discord-cya.glitch.me/');
}, 120000);

var Push = require('pushover-notifications');
var p = new Push({
	user: process.env.PUSHOVER_USER,
	token: process.env.PUSHOVER_TOKEN
});

const USER_ID = process.env.PERSONAL_ID;
const SERVER_ID = process.env.LOG_SERVER_ID;

const Discord = require('discord.js');
const user = new Discord.Client();
user.login(process.env.PERSONAL_TOKEN);
const bot = new Discord.Client();
bot.login(process.env.BOT_TOKEN);

bot.channel_associations = JSON.parse(fs.readFileSync('./associations.json', 'utf8'));

user.on('message', msg => {
	if (msg.channel.type != "dm") return;

	var SERVER = bot.guilds.get(SERVER_ID);
	var copy_channel = bot.channel_associations.find(association => association.original == msg.channel.id);

	var attachment_urls = []
	msg.attachments.forEach(attachment => {
		attachment_urls.push(attachment.url);
	});

	if (copy_channel == undefined) {
		var name = msg.channel.name;
		var target_id = msg.author.id;
		if (msg.author.id == USER_ID) {
			name = msg.channel.recipient.username;
			target_id = msg.channel.recipient.id;
		}
		SERVER.createChannel(name, 'text').then(new_channel => {
			new_channel.setTopic(target_id);
			bot.channel_associations.push({
				original: msg.channel.id,
				copy: new_channel.id
			});
			new_channel.send(msg.author.username + ": " + msg.cleanContent, {
				files: attachment_urls
			});
			fs.writeFileSync('associations.json', JSON.stringify(bot.channel_associations), 'utf8');
		});
	} else {
		SERVER.channels.get(copy_channel.copy).send(msg.author.username + ": " + msg.cleanContent, {
			files: attachment_urls
		});
	}
});

user.on('messageDelete', msg => {
	if (msg.channel.type != "dm") return;

	if (msg.author.id == USER_ID) return;

	var msg = {
		message: msg.cleanContent,
		title: msg.author.username + ": Message Deleted"
	}

	p.send(msg)
});
